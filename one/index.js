const count = () => 
{
    let i = 0
    return () => {
        return i++
    }
}
const counter = count()

console.log(counter())
console.log(counter())
console.log(counter())

/*
    The counter variable only exists in the function count() scope, avoiding the global variable solution
    Every time you call the function counter, it increment the value of i
*/