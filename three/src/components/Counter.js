import React, {useState} from 'react';

const Counter = () => {

    const [number, setNumber] = useState(0);
    const increment = () => {
        setNumber(number+1)
    }

    return ( 
        <div>
            <h1>{number}</h1>
            <button onClick={increment}>
                Counter
            </button>
        </div>
     );
}
 
export default Counter;