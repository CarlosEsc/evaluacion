const { Router } = require('express')
const router = new Router()

router.post('/palindrome', (req, res) => {
    console.log(req.body)
    const { phrase } = req.body
    console.log(typeof phrase)
    let str = phrase.toLowerCase()
    str = phrase.normalize('NFD').replace(/[\u0300-\u036f]/g,"").toLowerCase().replace(/[\W_]/g, '')

    const palindrome = str === [...str].reverse().join('');
    res.json({palindrome})
})

module.exports = router