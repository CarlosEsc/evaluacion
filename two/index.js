const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const port = 3000

app.set('port', process.env.PORT || port)

app.use(express.urlencoded({extended: false}))
app.use(bodyParser.json())
app.use(require('./routes/index'))

app.listen(app.get('port'), () => {
    console.log(`Carlos, your app is listening at localhost:${app.get('port')}`)
})